# На этой неделе в KDE: Discover redesign has begun

> 30 января – 6 февраля, основное — прим. переводчика

We have put the finishing touches on Plasma 5.24 and started to work on 5.25 stuff, with two big improvements already merged: keyboard navigation for panels, and the start of Discover’s UI redesign! Check those out below:

## Исправленные «15-минутные ошибки»

Current number of bugs: **83, same as last week**. [Current list of bugs](https://tinyurl.com/plasma-15-minute-bugs)

Plasma, Discover, and many other apps [no longer sometimes always crash on launch when you’ve turned on User Feedback sharing](https://bugs.kde.org/show_bug.cgi?id=449505) (Aleix Pol Gonzalez, KUserFeedback 1.1.0)

Changing user properties in the System Settings’ accounts page [once again works if you happen to have version 22.04.64 or newer of the `AccountsService` package](https://bugs.kde.org/show_bug.cgi?id=449385) (Jan Blackquill, Plasma 5.24)

Discover [no longer sometimes randomly freezes when you’re looking at an app’s details](https://bugs.kde.org/show_bug.cgi?id=449583) (Aleix Pol Gonzalez, Plasma 5.24.1)

## Wayland

In the Plasma Wayland session, Kate [no longer flashes when you hit Ctrl+S to save your changes](https://bugs.kde.org/show_bug.cgi?id=435361) (Christoph Cullmann, Kate 22.04)

In the Wayland session, dragging-and-dropping various things to XWayland apps [no longer sometimes makes them stop accepting clicks until the system is restarted](https://bugs.kde.org/show_bug.cgi?id=449362) (David Redondo, Plasma 5.24)

Spectacle’s Rectangular Region overlay [now appears above all full screen windows, not just some of them](https://invent.kde.org/plasma/kwin/-/merge_requests/1957) (Влад Загородний, Plasma 5.24)

## Other Bugfixes & Performance Improvements

Dolphin [no longer crashes when you cancel an archiving job in the middle that was initiated from one of Dolphin’s context menu «Compress» items](https://bugs.kde.org/show_bug.cgi?id=446926) (Méven Car, Ark 21.12.3)

When browsing an FTP server in Dolphin, opening files [once again opens them in the correct app rather than your web browser](https://bugs.kde.org/show_bug.cgi?id=443253) (Nicolas Fella, Dolphin 21.12.3)

When dragging-and-dropping items onto the desktop, [now all of them are placed at the dragged location, rather than only one of them being placed there and all the other ones being placed after other icons](https://bugs.kde.org/show_bug.cgi?id=445441) (Severin Von Wnuck, Plasma 5.24)

Discover [no longer crashes when you install or uninstall more than one Flatpak apps at once](https://bugs.kde.org/show_bug.cgi?id=440877) (Aleix Pol Gonzalez, Plasma 5.24)

Discover [now shows the correct size for very large packages](https://invent.kde.org/plasma/discover/-/merge_requests/245) (Jonas Knarbakk, Discover 5.24)

In the Plasma X11 session, using 30-bit color [now works](https://bugs.kde.org/show_bug.cgi?id=423014) (Xaver Hugl, Plasma 5.24)

The System Tray’s popup [now has the correct background color when the widget is located on the desktop rather than on a panel](https://bugs.kde.org/show_bug.cgi?id=449535) (Иван Ткаченко, Plasma 5.24.1)

The Battery & Brightness applet [no longer inappropriately shows the «Low Battery» icon when the only batteries present are from external wireless devices with an adequate charge level](https://bugs.kde.org/show_bug.cgi?id=448797) (Aleix Pol Gonzalez, Plasma 5.25)

KIO [no longer inappropriately tries and fails to handle non-file-based URLs registered to apps](https://invent.kde.org/frameworks/kio/-/merge_requests/719) (e.g. `tg://` for Telegram or `mailto://` for your email client) when the apps advertise that they accept URLs (Nicolas Fella, Frameworks 5.91)

KWin’s keyboard shortcuts (e.g. `Alt+Tab`) [no longer sometimes break after KWin is restarted](https://bugs.kde.org/show_bug.cgi?id=448369) (Влад Загородний, Frameworks 5.91)

When using a dark color scheme, the Breeze icon for the KDE Plasma logo [no longer partially disappears at large sizes](https://bugs.kde.org/show_bug.cgi?id=416302) (Gabriel Knarlsson, Frameworks 5.91)

Fixed [a couple of inconsistencies and glitches in various Breeze folder and mimetype icons](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/198) (Gabriel Knarlsson, Frameworks 5.91)

## Улучшения пользовательского интерфейса

You [can now drag tabs from one Kate to another](https://bugs.kde.org/show_bug.cgi?id=426768) (Waqar Ahmed, Kate 22.04)

Okular’s Bookmarks sidebar page [now has an improved UI, with buttons that have text and an «Add Bookmark» context menu item](https://invent.kde.org/graphics/okular/-/merge_requests/488) (Nate Graham, Okular 22.04):

![0](https://pointieststick.files.wordpress.com/2022/02/screenshot_20210924_151258.png)

When compressing multiple files from Dolphin’s context menu, the menu [now tells you the name of the resulting archive](https://bugs.kde.org/show_bug.cgi?id=446728) (Fushan Wen, Ark 22.04)

You [can now find Konsole by searching for «cmd» or «command prompt»](https://invent.kde.org/utilities/konsole/-/merge_requests/592) («M B», Konsole 22.04)

When searching for System Settings pages, [exact title matches are now weighted much more heavily](https://invent.kde.org/plasma/systemsettings/-/merge_requests/123) (Alexander Lohnau, Plasma 5.24)

You [can no longer use Discover to uninstall itself](https://bugs.kde.org/show_bug.cgi?id=449260) (Nate Graham, Plasma 5.24)

Discover’s App page [has been redesigned for greater aesthetics and usability](https://invent.kde.org/plasma/discover/-/merge_requests/246) (Nate Graham and Manuel Jésus de la Fuente, Plasma 5.25):

* ![1](https://pointieststick.files.wordpress.com/2022/02/desktop_-_krita.png)
* ![2](https://pointieststick.files.wordpress.com/2022/02/desktop_-_endless_sky.png)

You can now [use the new default Meta+Alt+P shortcut to cycle keyboard focus between your panels and activate applets with the Keyboard](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/641) (Marco Martin, Plasma 5.25)

The clipboard applet’s settings window [is now much more comprehensible](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1065) (Jonathan Marten, Plasma 5.25)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу новую инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Богдан Данильченко (expwez)](https://t.me/expwez)  
_Источник:_ https://pointieststick.com/2022/02/04/this-week-in-kde-discover-redesign-has-begun/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение -->
<!-- 💡: system tray → системный лоток -->
