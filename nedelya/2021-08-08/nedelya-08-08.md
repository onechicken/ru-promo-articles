# На этой неделе в KDE: Stability

> 1–8 августа, основное — прим. переводчика

This was a major bugfix week, with many important fixes to our core apps as well as the touchscreen experience. More of these are in the pipeline too! We are really trying to improve the stability of our software now that it’s starting to be used in more 3rd-party products like the [Steam Deck](https://www.steamdeck.com/ru/). The idea is that this will become a virtuous circle of better more stable products leading to more use leading to even better more stable products! So check it out:

## Новые возможности

Kate [now lets you open multiple tabs in its embedded terminal views](https://invent.kde.org/utilities/kate/-/merge_requests/470) (Waqar Ahmed, Kate 21.12)

It’s now possible to [configure whether hidden files are shown before or after other files in Dolphin, and the default is «before», as it used to be](https://invent.kde.org/system/dolphin/-/merge_requests/252) (Chris Holland, Dolphin 21.12)

You [can now delete selected items in the Clipboard applet’s popup by pressing the Delete key on the keyboard](https://bugs.kde.org/show_bug.cgi?id=439174) (Nate Graham, Plasma 5.23)

The «Get new [thing]» window [now has a feature to let you begin the process of uploading your own contributions to store.kde.org!](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/132) (Dan Leinir Turthra Jensen, Frameworks 5.85):

![1](https://pointieststick.files.wordpress.com/2021/08/contribute.png)

*And yes, we know that connectivity to store.kde.org has been flaky lately. I’ve been informed that the relevant sysadmins are working on it!*

## Исправления ошибок и улучшения производительности

Dolphin once again [restores the window and sidebar to the correct sizes after being unmaximized](https://bugs.kde.org/show_bug.cgi?id=430521) (Felix Ernst, Dolphin 21.08)

Elisa’s desktop config window [is now able to scroll vertically in situations where this would be required](https://bugs.kde.org/show_bug.cgi?id=440491), for example due to long translated test or many configured search locations for the music library (Nate Graham, Elisa 21.08)

Konsole [no longer sometimes crashes when closing a tab](https://bugs.kde.org/show_bug.cgi?id=411962) (Ahmad Samir, Konsole 21.12)

Fixed various recent regressions affecting Yakuake: it once again [slides out properly](https://bugs.kde.org/show_bug.cgi?id=435378) and [no longer flashes blue while closing](https://bugs.kde.org/show_bug.cgi?id=438458) (Влад Загородний, Plasma 5.22.5)

Fixed [a case where KWin could crash when pressing Alt+Tab to activate the Task Switcher](https://bugs.kde.org/show_bug.cgi?id=440318) (David Edmundson, Plasma 5.23)

In the Plasma X11 session, [touchscreen input now works properly when the Wacom System Settings module is installed](https://bugs.kde.org/show_bug.cgi?id=440556) (Nate Graham, Plasma 5.23)

In the Plasma Wayland session, clicking in a virtual machine window [now results in the click targeting the correct region of the screen in the guest OS](https://bugs.kde.org/show_bug.cgi?id=427060) (Андрей Бутырский, Plasma 5.23)

Discover [is now faster to launch](https://invent.kde.org/plasma/discover/-/merge_requests/138), especially on low-resource devices like the PinePhone (Aleix Pol Gonzalez, Plasma 5.23)

You [can now enter decimal values for manual chart data ranges in System Monitor](https://bugs.kde.org/show_bug.cgi?id=434628) (Arjen Hiemstra, Plasma 5.23)

Items on the desktop [once again get thumbnails automatically generated for them](https://bugs.kde.org/show_bug.cgi?id=438691) (Marcin Gurtowski, Frameworks 5.85)

Plasma text fields [now always have the correct text color even when using themes which use strongly contrasting colors for the window background vs the view background, such as Oxygen](https://bugs.kde.org/show_bug.cgi?id=438854) (Nate Graham, Frameworks 5.85)

Close buttons in Kirigami inline messages [no longer overlap the action button beneath them when using certain font sizes](https://bugs.kde.org/show_bug.cgi?id=440604) (Nate Graham, Frameworks 5.85)

The Breeze icon theme [is no longer missing network and hibernation-related icons when used in XFCE](https://bugs.kde.org/show_bug.cgi?id=432333) (Nate Graham, Frameworks 5.85)

## Улучшения пользовательского интерфейса

When you press-and-hold on a desktop widget with a finger on the touchscreen, [the icons in the overlay are now sized in a manner appropriate for touch interaction](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/537) (Nate Graham, Plasma 5.22.5):

![4](https://pointieststick.files.wordpress.com/2021/08/big-icons-for-touch-users.png)

The DrKonqi crash reporter [no longer lets users waste their own time by filing worthless bug reports against unmaintained apps and ancient unmaintained versions of developed apps, and instead recommends that they find a new app or upgrade, respectively](https://bugs.kde.org/show_bug.cgi?id=436549) (Harald Sitter, Plasma 5.23)

The System Settings Login Screen page’s settings synchronization feature [has now been renamed to «Apply Plasma Settings» to clarify what it does](https://bugs.kde.org/show_bug.cgi?id=439217) (Nate Graham, Plasma 5.23):

![5](https://pointieststick.files.wordpress.com/2021/08/apply-plasma-settings-text.png)

Dialogs for various file operations [now word-wrap the text so they never become too wide and get cut off when displaying extremely long file paths](https://bugs.kde.org/show_bug.cgi?id=440545) (Ahmad Samir, Frameworks 5.85)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/08/06/this-week-in-kde-stability/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: icons → значки -->
<!-- 💡: locations → расположения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
